<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('stripe_id')->nullable()->collation('utf8mb4_bin');
            $table->string('card_brand')->nullable();
            $table->string('card_last_four', 4)->nullable();
            $table->timestamp('trial_ends_at')->nullable();
            $table->timestamps();
        });

        // Schema::table('users', function (Blueprint $table) {
        //     $table->dropColumn(['stripe_id', 'card_brand', 'card_last_four', 'trial_ends_at']);
        // });

        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('account_id');
        });

        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->unsignedInteger('account_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropColumn('account_id');
            $table->unsignedInteger('user_id');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('account_id');
        });

        // Schema::table('users', function (Blueprint $table) {
        //     $table->string('stripe_id')->nullable()->collation('utf8mb4_bin');
        //     $table->string('card_brand')->nullable();
        //     $table->string('card_last_four', 4)->nullable();
        //     $table->timestamp('trial_ends_at')->nullable();
        // });

        Schema::dropIfExists('accounts');
    }
}
