<?php

use App\Plan;
use Illuminate\Database\Seeder;

class PlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $basic = [
            'name' => 'Basic',
            'slug' => 'basic',
            'stripe_plan' => 'plan_GFu5vqIKnSxihF',
            'cost' => 10.00,
        ];

        $premium = [
            'name' => 'Premium',
            'slug' => 'premium',
            'stripe_plan' => 'plan_GFu5aOV8VFk3R0',
            'cost' => 50.00,
        ];

        Plan::insert([$basic, $premium]);
    }
}
