<?php

namespace App;

use Laravel\Cashier\Billable;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use Billable;
}
