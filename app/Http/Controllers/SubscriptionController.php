<?php

namespace App\Http\Controllers;

use App\Plan;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    public function create(Request $request, Plan $plan)
    {
        $plan = Plan::findOrFail($request->get('plan'));
        $user = $request->user();
        $account = $user->account;
        $paymentMethod = $request->paymentMethod;

        // $user->createOrGetStripeCustomer();
        $account->createOrGetStripeCustomer();

        // if (!$user->hasPaymentMethod()) {
        //     $user->addPaymentMethod($paymentMethod);
        // } else {
        //     $user->updateDefaultPaymentMethod($paymentMethod);
        // }
        if (!$account->hasPaymentMethod()) {
            $account->addPaymentMethod($paymentMethod);
        } else {
            $account->updateDefaultPaymentMethod($paymentMethod);
        }

        // $user->newSubscription('main', $plan->stripe_plan)
        //     ->create($paymentMethod);
        $account->newSubscription('main', $plan->stripe_plan)
            ->create($paymentMethod);

        // $user->invoiceFor('One Time Setup Fee', 9999);
        $account->invoiceFor('One Time Setup Fee', 9999);

        return redirect()->route('home')->with('success', 'Your plan subscribed successfully');
    }
}
