@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div>
        <p>You will be charged ${{ number_format($plan->cost, 2) }} for {{ $plan->name }} Plan</p>
      </div>
      <div class="card">
        <form action="{{ route('subscription.create') }}" method="post" id="payment-form">
          @csrf
          <div class="form-group">
            <div class="card-header">
              <label for="card-element">
                Enter your credit card information
              </label>
            </div>
            <div class="card-body">
              <div id="card-element">
                <!-- A Stripe Element will be inserted here. -->
              </div>
              <!-- Used to display form errors. -->
              <div id="card-errors" role="alert"></div>
              <input type="hidden" name="plan" value="{{ $plan->id }}" />
            </div>
          </div>
          <div class="card-footer">
            <button id="card-button" class="btn btn-dark" data-secret="{{ $intent->client_secret }}">Pay</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script src="https://js.stripe.com/v3/"></script>
<script>
  $('document').ready(function(){
    // Custom styling can be passed to options when creating an Element.
    var style = {
      base: {
        color: '#32325d',
        lineHeight: '18px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };

    // Create a Stripe client.
    const stripe = Stripe('{{ env("STRIPE_KEY") }}');
    // Create an instance of Elements.
    const elements = stripe.elements();
    // Create an instance of the card Element.
    const cardElement = elements.create('card', {style: style});
    const cardButton = document.getElementById('card-button');
    const clientSecret = cardButton.dataset.secret;

    // Add an instance of the card Element into the `card-element` <div>.
    cardElement.mount('#card-element');

    // Handle real-time validation errors from the card Element.
    cardElement.addEventListener('change', function(event) {
      var displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });

  // Handle form submission.
  var form = document.getElementById('payment-form');
  form.addEventListener('submit', async (e) => {
    e.preventDefault();

    const { paymentMethod, error } = await stripe.createPaymentMethod(
        'card', cardElement, {
            // billing_details: { name: cardHolderName.value }
        }
    );

    if (error) {
        console.log(error.message);
    } else {
        stripeTokenHandler(paymentMethod);
    }



    // stripe.handleCardSetup(
    //   clientSecret, cardElement, {
    //     payment_method_data: {
    //       // billing_details: { name: cardHolderName.value }
    //     }
    //   }).then(function(result) {
    //     if(result.error) {
    //       console.log('error');
    //       var errorElement = document.getElementById('card-errors');
    //       errorElement.textContent = result.error.message;
    //     } else {
    //       console.log('no error');
    //       stripeTokenHandler(result.setupIntent.payment_method);
    //     }
    //   });
      
      // Submit the form with the token ID.
      function stripeTokenHandler(paymentMethod) {
        console.log(paymentMethod);
        // Insert the token ID into the form so it gets submitted to the server
      var form = document.getElementById('payment-form');
      var hiddenInput = document.createElement('input');
      hiddenInput.setAttribute('type', 'hidden');
      hiddenInput.setAttribute('name', 'paymentMethod');
      hiddenInput.setAttribute('value', paymentMethod.id);
      form.appendChild(hiddenInput);
      
      // Submit the form
      form.submit();
      }
    })
  });
</script>
@endsection